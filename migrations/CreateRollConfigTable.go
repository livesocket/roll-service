package migrations

import "github.com/jmoiron/sqlx"

func CreateRollConfigTable(tx *sqlx.Tx) error {
	_, err := tx.Exec("CREATE TABLE `roll_config` (`channel` varchar(255) NOT NULL,`enabled` TINYINT NOT NULL DEFAULT 0,`cooldown` INT UNSIGNED NOT NULL DEFAULT 0,`updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY(`channel`))")
	return err
}
