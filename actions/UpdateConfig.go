package actions

import (
	"errors"
	"log"

	"github.com/gammazero/nexus/v3/client"
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/roll-service/helpers"
	"gitlab.com/livesocket/roll-service/models"
	"gitlab.com/livesocket/service/lib"
)

// UpdateConfigAction Updates a channel roll config
//
// public.roll.config.update
// {channel string, enabled bool, cooldown uint}
//
// Returns [RollConfig]
var UpdateConfigAction = lib.Action{
	Proc:    "public.roll.config.update",
	Handler: updateConfig,
}

type updateConfigInput struct {
	Channel  string
	Enabled  bool
	Cooldown uint
}

func updateConfig(invocation *wamp.Invocation) client.InvokeResult {
	// Get input args from call
	config, err := getUpdateConfigInput(invocation.ArgumentsKw)
	if err != nil {
		return lib.ErrorResult(err)
	}

	// UpdateConfig
	new, err := config.Update()
	if err != nil {
		return lib.ErrorResult(err)
	}

	// Emit config updated
	err = helpers.EmitRollConfigUpdated(config, new)
	if err != nil {
		// Don't fail on error
		log.Print(err)
	}

	// Return updated config
	return lib.ArgsResult(new)
}

func getUpdateConfigInput(kwargs wamp.Dict) (*models.RollConfig, error) {
	if kwargs["channel"] == nil {
		return nil, errors.New("Missing channel")
	}
	if kwargs["enabled"] == nil {
		return nil, errors.New("Missing enabled")
	}
	if kwargs["cooldown"] == nil {
		return nil, errors.New("Missing cooldown")
	}

	return &models.RollConfig{
		Channel:  kwargs["channel"].(string),
		Enabled:  kwargs["enabled"].(bool),
		Cooldown: kwargs["cooldown"].(uint),
	}, nil
}
