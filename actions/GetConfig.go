package actions

import (
	"errors"

	"github.com/gammazero/nexus/v3/client"
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/roll-service/models"
	"gitlab.com/livesocket/service/lib"
)

// GetConfigAction Gets roll config for channel
//
// public.roll.config.get
// {channel string}
//
// Returns [RollConfig]
var GetConfigAction = lib.Action{
	Proc:    "public.roll.config.get",
	Handler: getConfig,
}

type getConfigInput struct {
	Channel string
}

func getConfig(invocation *wamp.Invocation) client.InvokeResult {
	// Get input args from call
	input, err := getGetConfigInput(invocation.ArgumentsKw)
	if err != nil {
		return lib.ErrorResult(err)
	}

	// Find config
	config, err := models.FindRollConfig(input.Channel)
	if err != nil {
		return lib.ErrorResult(err)
	}

	// Return roll config
	return lib.ArgsResult(config)
}

func getGetConfigInput(kwargs wamp.Dict) (*getConfigInput, error) {
	if kwargs["channel"] == nil {
		return nil, errors.New("Missing channel")
	}

	return &getConfigInput{
		Channel: kwargs["channel"].(string),
	}, nil
}
