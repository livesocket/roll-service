package commands

import (
	"fmt"
	"math/rand"
	"strconv"
	"time"

	"github.com/gammazero/nexus/v3/client"
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/conv"
	"gitlab.com/livesocket/service/lib"
)

// RollCommand Rolls a random number
//
// command.roll
// !roll <number>
//
// Returns [string] as response for chat
var RollCommand = lib.Action{
	Proc:    "command.roll",
	Handler: roll,
}

type rollInput struct {
	Max int
}

func roll(invocation *wamp.Invocation) client.InvokeResult {
	// Get input args from call
	input, err := getRollInput(invocation)
	if err != nil {
		return lib.ErrorResult(err)
	}

	// Roll random number
	source := rand.NewSource(time.Now().UnixNano())
	random := rand.New(source)
	result := random.Intn(input.Max)

	// Return message to display in chat
	return lib.ArgsResult(fmt.Sprintf("%d", result))
}

func getRollInput(invocation *wamp.Invocation) (*rollInput, error) {
	if len(invocation.Arguments) == 0 {
		return &rollInput{Max: 10}, nil
	}

	str := conv.ToString(invocation.Arguments[0])
	num, err := strconv.Atoi(str)
	if err != nil {
		return &rollInput{Max: 10}, nil
	}
	return &rollInput{Max: num}, nil
}
