package helpers

import (
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/roll-service/models"
	"gitlab.com/livesocket/service"
)

// EmitRollConfigUpdated Emits "event.roll.config.updated"
func EmitRollConfigUpdated(old *models.RollConfig, new *models.RollConfig) error {
	return service.Socket.Publish("event.roll.config.updated", nil, wamp.List{old, new}, nil)
}
