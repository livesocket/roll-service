package models

import (
	"fmt"
	"strings"
	"time"

	"gitlab.com/livesocket/service"
)

type RollConfig struct {
	Channel   string     `db:"channel" json:"channel"`
	Enabled   bool       `db:"enabled" json:"enabled"`
	Cooldown  uint       `db:"cooldown" json:"cooldown"`
	UpdatedAt *time.Time `db:"updated_at" json:"updated_at"`
}

func FindRollConfig(channel string) (*RollConfig, error) {
	config := RollConfig{}
	err := service.DB.Get(&config, "SELECT * FROM `roll_config` WHERE `channel`=?", channel)
	if err != nil {
		if strings.Contains(fmt.Sprint(err), "no rows") {
			return nil, nil
		}
		return nil, err
	}
	return &config, nil
}

func (c *RollConfig) Update() (*RollConfig, error) {
	now := time.Now()
	c.UpdatedAt = &now
	_, err := service.DB.NamedExec("UPDATE `roll_config` SET `enabled`=:enabled,`cooldown`=:cooldown,`updated_at`=:updated_at WHERE `channel`=:channel", c)
	if err != nil {
		return nil, err
	}
	return c, nil
}
