package main

import (
	"log"
	"os"
	"os/signal"

	"gitlab.com/livesocket/roll-service/actions"
	"gitlab.com/livesocket/roll-service/commands"
	"gitlab.com/livesocket/roll-service/migrations"
	"gitlab.com/livesocket/service"
	"gitlab.com/livesocket/service/lib"
)

func main() {
	close, err := service.NewStandardService([]lib.Action{
		actions.GetConfigAction,
		actions.UpdateConfigAction,
		commands.RollCommand,
	}, nil, "__roll_service", migrations.CreateRollConfigTable)
	defer close()
	if err != nil {
		panic(err)
	}

	// Wait for CTRL-c or client close while handling events.
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt)
	select {
	case <-sigChan:
	case <-service.Socket.Done():
		log.Print("Router gone, exiting")
		return
	}
}
